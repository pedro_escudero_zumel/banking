class CreateBankAccounts < ActiveRecord::Migration
  def change
    create_table :bank_accounts do |t|
      t.integer :balance, default: 0
      t.integer :user_id
      t.timestamps null: false
    end
  end
end
