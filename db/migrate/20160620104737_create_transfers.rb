class CreateTransfers < ActiveRecord::Migration
  def change
    create_table :transfers do |t|
      t.integer :origin_user_id
      t.integer :destination_user_id
      t.integer :amount
      t.integer :origin_user_balance
      t.integer :destination_user_balance
      t.timestamps null: false
    end
  end
end
