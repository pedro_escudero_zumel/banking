class TransfersController < ApplicationController
  before_action :set_transfer, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /transfers
  # GET /transfers.json
  def index
    @transfers = BankAccount.where(user_id: current_user.id)
  end

  # GET /transfers/1
  # GET /transfers/1.json
  def show
    @transfers = Transfer.select{|transfer| transfer.destination_user_id == 1 || transfer.origin_user_id == 1}
  end

  # GET /transfers/new
  def new
    @transfer = Transfer.new(origin_user_id: current_user.id, origin_user_balance: current_user.bank_account.balance)
  end

  # GET /transfers/1/edit
  def edit
  end

  # POST /transfers
  # POST /transfers.json
  def create
    binding.pry
    @transfer = Transfer.new(transfer_params)
    @transfer.destination_user_balance = User.find(transfer_params["destination_user_id"]).bank_account.balance + transfer_params["amount"].to_i
    @transfer.origin_user_balance = transfer_params["origin_user_balance"].to_i - transfer_params["amount"].to_i
    binding.pry
    respond_to do |format|
      if @transfer.save
        format.html { redirect_to @transfer, notice: 'Transfer was successfully created.' }
        format.json { render :show, status: :created, location: @transfer }
      else
        format.html { render :new }
        format.json { render json: @transfer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /transfers/1
  # PATCH/PUT /transfers/1.json
  def update
    respond_to do |format|
      if @transfer.update(transfer_params)
        format.html { redirect_to @transfer, notice: 'Bank account was successfully updated.' }
        format.json { render :show, status: :ok, location: @transfer }
      else
        format.html { render :edit }
        format.json { render json: @transfer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transfers/1
  # DELETE /transfers/1.json
  def destroy
    @transfer.destroy
    respond_to do |format|
      format.html { redirect_to transfers_url, notice: 'Bank account was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transfer
      @transfer = BankAccount.find_by(user_id: current_user.id) if current_user
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def transfer_params
      params.require(:transfer).permit(:amount, :destination_user_id, :origin_user_id, :origin_user_balance)
    end

end