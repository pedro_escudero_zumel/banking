module UsersHelper

  def users_to_transfer
    User.select{|user| user != current_user}.collect {|u| [u.name, u.id]}
  end

end
