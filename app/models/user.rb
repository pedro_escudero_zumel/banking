class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_one :bank_account
  has_many :transfers

  after_create :create_bank_account

  validates :name, length: {maximum: 255}
  validates :email, length: {maximum: 255}

  def transfer_to(destination_user_id)
    Transfer.new.perform_transfer(self.id, destination_user_id)
  end

  def add_credit(amount)
    if amount.kind_of? Integer
      self.bank_account.update(balance: self.bank_account.balance + amount) 
      Transfer.create(origin_user_id: id, destination_user_id: id,
       origin_user_balance: self.bank_account.balance - amount, destination_user_balance: self.bank_account.balance,
        amount: amount)
    else
      'wrong amount'  
    end
  end

  private

  def create_bank_account
    BankAccount.create!(balance: 0, user_id: id)
  end  

end
