class Transfer < ActiveRecord::Base

   belongs_to :origin_user, :class_name => 'User'
   belongs_to :destination_user, :class_name => 'User'

   after_save :transfer_money

   validates_presence_of :origin_user_balance, :destination_user_balance, :amount, :origin_user_id, :destination_user_id

   def line_balance
     origin_user == destination_user ? destination_user_balance : origin_user_balance
   end

   private

   def transfer_money
      BankAccount.find(destination_user_id).update(balance: BankAccount.find(destination_user_id).balance + amount)
      BankAccount.find(origin_user_id).update(balance: BankAccount.find(origin_user_id).balance - amount)
   end

end
